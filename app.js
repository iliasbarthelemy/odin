/**
* Application configuration
*/
var admin_password = 'atlas';
var listen_port = 80;
var environment = 'development';
var database_dsn = 'mongodb://localhost/odin';

/**
 * Module dependencies.
 */
var express = require('express'),
    app = express(),
    routes = require('./routes'),
    http = require('http').createServer(app),
    io = require('socket.io').listen(http),
    mongoose = require('mongoose'),
    path = require('path'),
    fs = require('fs');

var users = {};

// all environments
app.set('port', process.env.PORT || listen_port);
app.set('env', environment);
app.set('views', __dirname + '/views');
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

http.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

mongoose.connect(database_dsn,function(err){
	if(err){
  		console.log('Error: ' + err);
	}else if ('development' == app.get('env')) {
  		console.log('Connected to mongodb');
	}
});

var chatSchema = mongoose.Schema({
	user: String,
	msg: String,
	created: {type: Date, default: Date.now}
});

var chat = mongoose.model('Message',chatSchema);

app.get('/', function(request,response){
  response.sendfile(__dirname + '/views/index.html');
});

io.sockets.on('connection',function(socket){
	var query = chat.find({});
	query.sort('-created').limit(20).exec(function(err, docs){
		if(err) throw err;
		socket.emit('old_msg_data', docs);

		if ('development' == app.get('env')) {
   		    console.log('Sending old messages');
   		}
	});

	socket.on('new_nick_data',function(nick_data,callback){
		if(nick_data.user in users){
			callback(false);
		}else{
			socket.nickname = nick_data.user;
            if(nick_data.password == admin_password){
                socket.is_admin = true;
                socket.emit('is_admin',true);
                if ('development' == app.get('env')) {
                    console.log('New user: ' + nick_data.user+' (ADMIN)');
                }
            }else{
                socket.is_admin = false;
                if ('development' == app.get('env')) {
                    console.log('New user: ' + nick_data.user+' Password: ' + nick_data.password);
                }
            }

			users[socket.nickname] = socket;
			updateUsers();
			callback(true);
		}

	});

	socket.on('container_close',function(to_close,callback){
	    if(socket.is_admin){
	        if ('development' == app.get('env')) {
                console.log('Container: #' + to_close + ' closed by user: '+socket.nickname);
            }
            io.sockets.emit('container_close',to_close);
        }
	});

    socket.on('container_refresh',function(refresh_data,callback){
    	    if(socket.is_admin){
    	        if ('development' == app.get('env')) {
                    console.log('Container: #' + refresh_data.id + ' refreshing every '+ refresh_data.value +' seconds, assigned by user '+socket.nickname);
                }
                io.sockets.emit('container_refresh',refresh_data);
            }
    	});

	socket.on('container_data',function(container_data,callback){
	        if ('development' == app.get('env')) {
                console.log(	'Name:'+container_data.name+
					' Container: '+container_data.type+
					' Parameters:'+container_data.param+
					' Id:'+container_data.id+
					' Width: '+container_data.width+
					' Height: '+container_data.height+
					' Top: '+container_data.top+
					' Left: '+container_data.left+
					' Z-index: '+container_data.zindex);
            }

			fs.readFile('views/widgets/'+container_data.type+'/component.html', 'utf8', function (err,data) {
                if(socket.is_admin){
                    if (err) {
                        var data_obj = {};
                        data_obj['html'] = 'Error nr: '+err.errno;
                        data_obj['name'] = container_data.name;
                                    data_obj['type'] = container_data.type;
                                    data_obj['param'] = container_data.param;
                                    data_obj['id'] = container_data.id;
                                    data_obj['width'] = container_data.width;
                                    data_obj['height'] = container_data.height;
                                    data_obj['top'] = container_data.top;
                                    data_obj['left'] = container_data.left;
                                    data_obj['zindex'] =  container_data.zindex;
                        io.socket.emit('container_update',data_obj);
                        if ('development' == app.get('env')) {
                            console.log(err);
                        }
                    }else{
                        var data_obj = {};
                        data_obj['html'] = data;
                        data_obj['name'] = container_data.name;
                                    data_obj['type'] = container_data.type;
                                    data_obj['param'] = container_data.param;
                                    data_obj['id'] = container_data.id;
                                    data_obj['width'] = container_data.width;
                                    data_obj['height'] = container_data.height;
                                    data_obj['top'] = container_data.top;
                                    data_obj['left'] = container_data.left;
                                    data_obj['zindex'] =  container_data.zindex;
                        io.sockets.emit('container_update',data_obj);
                    }
				}
			});
        });


	function updateUsers(){
		io.sockets.emit('users_data',Object.keys(users));
	}		

	socket.on('chat_msg_data',function(chat_data, callback){
		var msg = chat_data.trim();
		if(msg.substr(0,3) == '/w '){
			var msg = msg.substr(3);
			var ind = msg.indexOf(' ');
			if(ind !== -1){
				var name_to = msg.substring(0,ind);
				var msg = msg.substring(ind+1);
				if(name_to in users){
					users[name_to].emit('private_msg',{msg: msg,user: socket.nickname});
					if ('development' == app.get('env')) {
   					    console.log('Chat private msg: ' + msg + ' from user: '+socket.nickname +' to user: '+name_to) ;
   					}
				}else{
					callback('Error, user not found');
				}
			}else{
				callback('Error, please enter a msg');
			}

		}else{
			var new_msg = new chat({msg: msg,user: socket.nickname});
			new_msg.save(function(err){
				if(err) throw err;
				io.sockets.emit('chat_data',{msg: msg,user: socket.nickname});
                if ('development' == app.get('env')) {
   				    console.log('Chat msg: ' + chat_data + ' from user: '+socket.nickname);
   				}
			});
		}
	});

	socket.on('disconnect',function(data){
		if(!socket.nickname) { return };
		delete users[socket.nickname];
		updateUsers();
	});
});
