jQuery(function($){

	function getMaxZIndex() {
    		var zIndexMax = 0;
    		$('div').each(function () {
        		var z = parseInt($(this).css('z-index'));
        		if (z > zIndexMax) zIndexMax = z;
  		});
    		return zIndexMax;
	}
	
	function getMaxContainerId() {
    		var max = 0;
    		$('.container').each(function () {
        		var z = parseInt($(this).attr('id'));
        		if (z > max) max = z;
  		});
    		return max+1;
	}

	var socket = io.connect();

	$("#widget_name").click(function(){
		$(this).val('');
	});
	
	$("#widget_param").click(function(){
		$(this).val('');
	});

	$("#widget_select").click(function(){
		if($(this).val() !== 'chat'){
			$('#widget_param').show();
		}else{
			$('#widget_param').hide();
		}
		$('#widget_name').focus();
	});

	 socket.on('container_update',function(con_data){

		if(con_data.type == 'url'){

		    var n_height = con_data.height.slice(0,-2) - 65;

            var new_div = $('#mokup').clone(true,true);
			new_div.attr('id',con_data.id);
			new_div.attr('class','container');
			new_div.find('h1').html(con_data.type+' - '+con_data.name);
			new_div.resizable({ animate: true,
		        	ghost: true,
            			handles: 'n,s,e,w,ne,se,nw,sw',
       			}).draggable({ containment: $("body") });
			new_div.css('height',con_data.height);
			new_div.css('width',con_data.width);
			new_div.css('top',con_data.top);
			new_div.css('left',con_data.left);
			new_div.css('z-index',con_data.zindex);
			new_div.find('.content').html(con_data.html);
			new_div.find('.content .inner_content').attr('src',con_data.param);
			new_div.find('.content .inner_content').css('height',n_height+"px");
			new_div.show();

			new_div.appendTo('body');
		}
     });


	$('#new_widget').submit(function(e){
                e.preventDefault();

		
        if($('#widget_select').val() == 'chat'){
			if($('#widget_name').val() == '' || $('#widget_name').val() == 'Name of the widget'){
				$('#widget_name').focus();
			}else{
                		$('#main_chat_container').show();
				$('#main_chat_container h1').html($('#widget_select').val()+' - '+$('#widget_name').val());
				resetForm();
			}
                }else{
			if($('#widget_name').val() == '' || $('#widget_name').val() == 'Name of the widget'){
				$('#widget_name').focus();
                        }else
			if($('#widget_param').val() == '' || $('#widget_param').val() == 'Parameters'){
				$('#widget_param').focus();
                        }else{
				var new_id = getMaxContainerId();
				var con_data = {};				
				con_data['type'] = $('#widget_select').val();
				con_data['param'] = $('#widget_param').val();
				con_data['name'] = $('#widget_name').val();
				con_data['id'] = new_id;
				con_data['height'] = '400px';
				con_data['width'] = '300px';
				con_data['left'] = '0px';
				con_data['top'] = '0px'
				con_data['zindex'] = getMaxZIndex();
				socket.emit('container_data',con_data ,function(data){
                		});

				resetForm();
			}
		}
        });

	function resetForm(){
		$('#widget_name').val('Name of the widget');
		$('#widget_param').val('Parameters');
		$("#widget_select option").removeAttr('selected').find(':first').attr('selected','selected');
		$('#widget_param').hide();
	}

	$('.container').resizable({ animate: true,
            ghost: true,
            handles: 'n,s,e,w,ne,se,nw,sw',
        }).draggable({ containment: $("body") });




	$(".close").click(function(){
		if($(this).parent().parent().attr('id') !== 'main_chat_container'){
			socket.emit('container_close',$(this).parent().parent().attr('id') ,function(){

			});
		}else{
			$(this).parent().parent().hide();
		}
	});

	 socket.on('container_close',function(to_remove){
         $('#'+to_remove).remove();
     });
	
	$(".min").click(function(){
		$('.max').show();
		if($('.state').val() == 'normal'){
			$(this).parent().parent().css('height','25px').css('overflow','hidden');
			$(this).hide();
			$('.state').val('min');
			$('.up').hide();
		}else{
			$(this).parent().parent().attr('class','');
			$('.state').val('normal');
			$('.up').show();
		}
	});
	
	$(".max").click(function(){
		$('.min').show();
		if($('.state').val() == 'normal'){
			$(this).parent().parent().attr('class','overlay');
			$('.state').val('max');
			$(this).hide();
			$('.up').hide();
		}else{
			$(this).parent().parent().css('height','auto').css('overflow','');
			$('.state').val('normal');
			$('.up').show();
		}
	});
	

	$(".container h1").click(function(){
		$(this).parent().css('z-index',getMaxZIndex()+1);
	});
});
