jQuery(function($){
    var socket = io.connect();

    $(document).on('change', '.url_refresh select', function() {
        var refresh_val = $(this).val();
        var to_refresh = $(this).parent().parent().parent().parent().attr('id');
        var data = {id: to_refresh,value: refresh_val}
        socket.emit('container_refresh',data ,function(){  });
	});

	socket.on('container_refresh',function(refresh_data){
        var src = $('#'+refresh_data.id).find('iframe').attr('src');
        setInterval($('#'+refresh_data.id).find('iframe').attr('src',src),refresh_data.value*10);
    });
});