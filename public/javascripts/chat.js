jQuery(function($){
	var 	socket = io.connect(),
		msg_data = $('#chat_msg_data'),
		nick_data = $('#nick_data');
		pass_data = $('#pass_data');

	$('#send_msg').submit(function(e){
		sendMsg();
		e.preventDefault();
	});

	$(document).keydown(function(event) {
    	var currKey=0,e=e||event; 
    	currKey=e.keyCode||e.which||e.charCode;  //do this handle FF and IE
    	if (!( String.fromCharCode(event.which).toLowerCase() == 's' && event.ctrlKey) && !(event.which == 19)) return true;
    	event.preventDefault();
		sendMsg();
    	return false;
	});  

	function sendMsg(){
		socket.emit('chat_msg_data',msg_data.val(),function(data){
                	$('#stream').append('<span class="error">'+data+'</span><br/>').animate({ scrollTop: $(document).height() }, "slow");
		});
		msg_data.val('');
	}	

	$('#set_nick').submit(function(e){
		e.preventDefault();
		socket.emit('new_nick_data',{user:nick_data.val(),password:pass_data.val()},function(nick_valid){
			if(nick_valid){
				$('#chat_container').show();
				$('#nick_container').hide();
				$('#user_container_button').show();
			}else{
				$('#error').html('User already exist');
			}
		});

		nick_data.val('');
	});

	socket.on('users_data',function(users){
		var html = '';
                for(var i=0; i < users.length; i++){
			html += '<div>'+users[i] + '</div>';
		}
		$('#user_container').html(html);

    });

    socket.on('disconnect',function(data){
        alert('Server going down.  Page will refresh automatically on reconnect.');
    });

    socket.on('reconnect',function(data){
        location.reload();
    });
 
	socket.on('chat_data',function(chat_data){
		displayMsg(chat_data);
    });

    socket.on('is_admin',function(is_admin){
    		$('#new_widget').show();
    		$('.container_control .close').show();
    });
	
	socket.on('old_msg_data',function(docs){
		for(var i=docs.length-1; i >= 0; i--){
			displayMsg(docs[i]);
		}
    });
	
	function displayMsg(chat_data){
		f_msg=chat_data.msg.replace(/\n/g,"<br>");	
                $('#stream').append('<span class="msg">'+chat_data.user+': </span>'+f_msg+'<br/>').animate({ scrollTop: $(document).height() }, "slow");
	}

	socket.on('private_msg',function(chat_data){
                $('#stream').append('<span class="private_msg">'+chat_data.user+': </span>'+chat_data.msg+'<br/>').animate({ scrollTop: $(document).height() }, "slow");
    });
	
	$("#user_container_button").click(function(){
		$(this).hide();
		$('#user_container').show();
	});

	$(document).on('click', '#user_container div', function() {
		$('#user_container').hide();
		$('#user_container_button').show();

		$(msg_data).focus().val('/w '+$(this).html()+' ');
	});

	$(msg_data).click(function(){
		$(this).val('');
	});
 
 
});
